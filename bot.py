import telebot
from telebot import types

#Ниже код модели

from sklearn.feature_extraction.text import CountVectorizer
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from collections import Mapping
from collections import defaultdict
from sklearn.metrics import classification_report
from nltk import ngrams
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from sklearn.metrics import accuracy_score
import pickle
import numpy as np
import pandas as pd
import time
from functools import wraps
from telebot import types
import logging

#Красное вино
def runModelRed(wine_description):
    with open('modelRed.pkl', 'rb') as fin:
        vectorizer, clf = pickle.load(fin)

    test = [wine_description]
    test_vec = vectorizer.transform(test)
    print(test_vec)
    pred_dict = dict(zip(clf.classes_, clf.predict_proba(test_vec)[0]))
    print(pred_dict)
    reversed_pred_dict = {}
    for key in pred_dict:
        index = pred_dict[key]
        reversed_pred_dict[index] = key

    pred_list = reversed_pred_dict.items()
    sorted_pred_list = sorted(pred_list, key=lambda x: x[0], reverse=True)

    red_wine_type_list = [val[1] for val in sorted_pred_list[:3]]

    return red_wine_type_list

   
def red_wine_variety(variety):
    wine_data = pd.read_csv('red_wines.csv')

    var_prediction = variety
    var_list = wine_data.loc[wine_data['grapes'] == var_prediction]

    sorted_df = var_list.sort_values(['average rating'], ascending=[False])
    wineries = sorted_df[['name', 'region', 'country']][0:2]
    var_prediction = variety
    var_list = wine_data.loc[wine_data['grapes'] == var_prediction]

    sorted_df = var_list.sort_values(['average rating'], ascending=[False])
    wineries = sorted_df[['name', 'region', 'country']][0:2]

    wines = ("Самый высокий рейтинг сорта " + var_prediction + " у следующих вин: " + wineries.iloc[0]['name'] \
             + " из " \
             + wineries.iloc[0]['region'] + ", " + wineries.iloc[0]['country'] + " и " \
             + wineries.iloc[1]['name'] + " из " \
             + wineries.iloc[1]['region'] + ", " + wineries.iloc[1]['country'] \
             + "\n")
    return wines


def reply_red(red_wine_type_list):
    repl = []
    for i in red_wine_type_list:
        repl.append(red_wine_variety(i))

    return repl


# Белое вино
def runModelWhite(wine_description):
    with open('modelWhite.pkl', 'rb') as fin:
        vectorizer, clf = pickle.load(fin)

    test = [wine_description]
    test_vec = vectorizer.transform(test)
    print(test_vec)
    pred_dict = dict(zip(clf.classes_, clf.predict_proba(test_vec)[0]))
    print(pred_dict)
    reversed_pred_dict = {}
    for key in pred_dict:
        index = pred_dict[key]
        reversed_pred_dict[index] = key

    pred_list = reversed_pred_dict.items()
    sorted_pred_list = sorted(pred_list, key=lambda x: x[0], reverse=True)

    white_wine_type_list = [val[1] for val in sorted_pred_list[:3]]

    return white_wine_type_list

    
def white_wine_variety(variety):
    wine_data = pd.read_csv('white_wines.csv')
    var_prediction = variety
    var_list = wine_data.loc[wine_data['grapes'] == var_prediction]

    sorted_df = var_list.sort_values(['average rating'], ascending=[False])
    wineries = sorted_df[['name', 'region', 'country']][0:2]
    var_prediction = variety
    var_list = wine_data.loc[wine_data['grapes'] == var_prediction]

    sorted_df = var_list.sort_values(['average rating'], ascending=[False])
    wineries = sorted_df[['name', 'region', 'country']][0:2]

    wines = ("Самый высокий рейтинг сорта " + var_prediction + " у следующих вин: " + wineries.iloc[0]['name'] \
             + " из " \
             + wineries.iloc[0]['region'] + ", " + wineries.iloc[0]['country'] + " и " \
             + wineries.iloc[1]['name'] + " из " \
             + wineries.iloc[1]['region'] + ", " + wineries.iloc[1]['country'] \
             + "\n")
    return wines


def reply_white(white_wine_type_list):
    repl = []
    for i in white_wine_type_list:
        repl.append(white_wine_variety(i))

    return repl





#БОТ:
API_TOKEN = 'TOKEN'

bot = telebot.TeleBot(API_TOKEN)

user_dict = {}


class User:
    def __init__(self, name):
        self.name = name
        self.age = None
        self.wine = None


@bot.message_handler(commands=['help', 'start'])
def process_name_step(message):
        chat_id = message.chat.id
        name = message.text
        user = User(name)
        user_dict[chat_id] = user
        msg = bot.reply_to(message, 'Сколько тебе лет?')
        bot.register_next_step_handler(msg, process_age_step)



def process_age_step(message):
        chat_id = message.chat.id
        age = message.text
        if not age.isdigit():
            msg = bot.reply_to(message, 'Число полных лет!')
            bot.register_next_step_handler(msg, process_age_step)
            user = user_dict[chat_id]
            user.age = age
            return
        if int(age) < 18:
            msg = bot.reply_to(message, 'Тебе еще рано!')
            bot.register_next_step_handler(msg, process_name_step)
        else:
            user = user_dict[chat_id]
            user.age = age
            msg = bot.reply_to(message, """\
            Опиши вино
            """)
            bot.register_next_step_handler(msg, process_ask_step)

def process_ask_step(message):
        chat_id = message.chat.id
        name = message.text
        user = User(name)
        user_dict[chat_id] = user
        markup = types.ReplyKeyboardMarkup(one_time_keyboard=True)
        markup.add('Красное', 'Белое')
        msg = bot.reply_to(message, 'Какое вино?', reply_markup=markup)
        bot.register_next_step_handler(msg, process_wine_step)


def process_wine_step(message):
        chat_id = message.chat.id
        wine = message.text
        user = user_dict[chat_id]
        if (wine == 'Красное'):
            chat_id = message.chat.id
            user.wine = wine
            user = user_dict[chat_id]
            print(user.name, user.age, user.wine)
            choice = runModelRed(user.name)
            choice_list = reply_red(choice)
            markdown = """
                *bold text*
                _italic text_
                [text](URL)
                """
            response = 'Рекомендуем вам: ' + "\n" + "*" + "\n".join(choice) + "*" "\n" + "\n"  "_" + "\n".join(
                choice_list) + "_"

            msg = bot.send_message(chat_id, text=response, parse_mode="Markdown")
            bot.register_next_step_handler(msg, process_name_step)

        else:
            chat_id = message.chat.id
            user.wine = wine
            user = user_dict[chat_id]
            print(user.name, user.age, user.wine)
            choice = runModelWhite(user.name)
            choice_list = reply_white(choice)
            markdown = """
                *bold text*
                _italic text_
                [text](URL)
                """

            response = 'Рекомендуем вам: ' + "\n" + "*" + "\n".join(choice) + "*" "\n" + "\n"  "_" + "\n".join(
            choice_list) + "_"

            msg = bot.send_message(chat_id, text=response, parse_mode="Markdown")
            bot.register_next_step_handler(msg, process_name_step)

bot.enable_save_next_step_handlers(delay=2)

bot.load_next_step_handlers()

bot.polling()



